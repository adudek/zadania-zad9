import os, sys

activate_this = '/home/p6/pyramidenv/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

from pyramid.paster import get_app, setup_logging
ini_path = '/home/p6/lab/development.ini'
setup_logging(ini_path)
application = get_app(ini_path, 'main')
