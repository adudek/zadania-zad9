# -*- coding: utf-8 -*-

import unittest
from pyramid import testing

####################
#   Testy modeli   #
####################

class BlogModelTests(unittest.TestCase):
    def _getTargetClass(self):
        from .models import Blog
        return Blog

    def _makeOne(self):
        return self._getTargetClass()

    def test_it(self):
        blog = self._makeOne()
        self.assertEqual(blog.__parent__, None)
        self.assertEqual(blog.__name__, 'Blog')

class PostModelTests(unittest.TestCase):
    def _getTargetClass(self):
        from .models import Post
        return Post

    def _makeOne(self, title, content):
        return self._getTargetClass()(title=title, content=content)

    def test_constructor(self):
        post = self._makeOne(u'test', u'test2')
        self.assertEqual(post.title, u'test')
        self.assertEqual(post.content, u'test2')

class CommentModelTests(unittest.TestCase):
    def _getCommentClass(self):
        from .models import Comment
        return Comment

    def _makeOne(self, text):
        return self._getCommentClass()(text=text)

    def test_constructor(self):
        comment = self._makeOne(u'test')
        self.assertEqual(comment.text, u'test')

#####################
#   Testy widokow   #
#####################

class ViewWikiTests(unittest.TestCase):
    def test_it(self):
        from .views import view_blog
        context = testing.DummyResource()
        request = testing.DummyRequest()
        response = view_blog(context, request)
        self.assertEqual(response.has_key('posts'), True)
        self.assertEqual(response['posts'], [])
        self.assertEqual(response.has_key('add_url'), True)
        self.assertEqual(response['add_url'], 'add')

class ViewPostTests(unittest.TestCase):
    def _callFUT(self, context, request):
        from .views import get_post
        return get_post(context, request)

    def test_it(self):
        blog = testing.DummyResource()
        blog['1'] = testing.DummyResource()
        context = testing.DummyResource(id='1', title=u'test', content=u'test2', comments_list={})
        context.__parent__ = blog
        context.__name__ = 'Post'
        request = testing.DummyRequest()
        info = self._callFUT(context, request)
        self.assertEqual(info['post'], context)

class AddPostsTests(unittest.TestCase):
    def _callFUT(self, context, request):
        from .views import add_post
        return add_post(context, request)

    def test_it_notsubmitted(self):
        context = testing.DummyResource()
        request = testing.DummyRequest()
        request.title = u'test'
        info = self._callFUT(context, request)
        self.assertEqual(info.location,'http://example.com/')

    def test_it_submitted(self):
        context = testing.DummyResource()
        request = testing.DummyRequest({'content': u'test', 'title': u'test2'}, post={'content': u'test', 'title': u'test2'})
        self._callFUT(context, request)

class AddCommentsTests(unittest.TestCase):
    def _callFUT(self, context, request):
        from .views import add_comment
        return add_comment(context, request)

    def test_it_notsubmitted(self):
        context = testing.DummyResource()
        request = testing.DummyRequest()
        request.title = 'test'
        info = self._callFUT(context, request)
        self.assertEqual(info.location,'http://example.com/')

    def test_it_submitted(self):
        from .models import Post
        context = Post(u'test1')
        context.__name__ = '1'
        request = testing.DummyRequest({'text': u'test1', 'author': u'test2'}, post={'text': u'test1', 'author': u'test2'})
        self._callFUT(context, request)
        page = context['1']
        self.assertEqual(page.text, u'test1')
        self.assertEqual(page.__name__, '1')
        self.assertEqual(page.__parent__, context)
