# -*- coding: utf-8 -*-

import re
from docutils.core import publish_parts
from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config
from .models import Blog, Post, Comment

wikiwords = re.compile(ur"\b([A-ZĆŁŚŹŻ]\w+[A-ZĆŁŚŹŻ]+\w+)", re.UNICODE)

@view_config(context='.models.Blog', renderer='templates/list.pt')
def view_blog(context, request):
    for row in context.values():
        row.comments = len(row.comments_list.keys())
        row.more = request.resource_url(row)

    post_list = sorted(context.values(), key=lambda x: x.created, reverse=True)[0:10]
    return {'posts': post_list, 'add_url': 'add'}


@view_config(context='.models.Post', renderer='templates/view.pt')
def get_post(context, request):
    return {
        'post': context,
        'comments': context.comments_list.values(),
        'add_url': 'comments'
    }

@view_config(name='add', context='.models.Blog')
def add_post(context, request):
    if request.method == 'POST':
        post = Post(request.params['title'], publish_parts(request.params['content'], writer_name='html')['html_body'])
        post.__name__ = str(len(context.keys()) + 1)
        post.__parent__ = context
        context[id] = post
    return HTTPFound(location=request.resource_url(context))

@view_config(name='comments', context='.models.Post')
def add_comment(context, request):
    if request.method == 'POST':
        comment = Comment(request.params['text'])
        comment.__name__ = str(len(context.comments_list.keys()) + 1)
        comment.__parent__ = context
        context.add(comment.__name__, comment)
    return HTTPFound(location=request.resource_url(context))