# -*- coding: utf-8 -*-

from persistent.mapping import PersistentMapping
from persistent import Persistent

import datetime

class Blog(PersistentMapping):
    __parent__ = None
    __name__ = None

class Post(Persistent):
    def __init__(self, title='', content=''):
    	self.title = title
    	self.content = content
    	self.created = datetime.datetime.now()
        self.comments_list = dict()

    def __getitem__(self, key):
        return self.comments_list[key]

    def add(self, key, comment):
        self.comments_list[key] = comment

class Comment(Persistent):
    def __init__(self, text=''):
        self.text = text

    def __getitem__(self, item):
        return None

def appmaker(zodb_root):
    if not 'app_root' in zodb_root:
        # root
        app_root = Blog()
        zodb_root['app_root'] = app_root
        import transaction
        transaction.commit()
    return zodb_root['app_root']
